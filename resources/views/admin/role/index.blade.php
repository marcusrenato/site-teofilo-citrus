@extends('adminlte::page')

@section('title', 'Cargos')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Cargos
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Cargos
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
@include('sweetalert::alert')

    <div class="card card-outline card-primary">
        <div class="card-header">
            <div class="card-title">
                Lista de cargos
            </div>
        </div>

        <div class="row justify-content-end mr-3 mt-3">
            <a href="{{ route('admin.role.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Adicionar
            </a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Ações</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td>
                                    {{ $role->id }}
                                </td>
                                <td>
                                    {{ $role->name }}
                                </td>
                                <td>
                                    @if ($role->id !== 1)
                                        @can('editar cargo')
                                            <a href="{{ route('admin.role.edit', $role->id) }}" class="btn  btn-sm btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endcan

                                        @can('excluir cargo')
                                            <form action="{{ route('admin.role.destroy', $role->id) }}" class="d-inline" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir?')">
                                                    <i class="fa fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        @endcan
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
