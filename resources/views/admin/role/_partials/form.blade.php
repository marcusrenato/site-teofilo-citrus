@csrf

<div class="form-group">
    <label for="name">Nome *</label>
    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $role->name ?? old('name') }}">

    @error('name')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>

<div class="form-group clearfix">
    <label for="permissions">Permissões</label>

    @foreach ($permissions as $permission)
        <div class="icheck-success">
            <input value="{{ $permission->name }}" type="checkbox" class="custom-control-input" id="permission-{{ $permission->name }}" name="permissions[]" @if (isset($role)) @if($role->permissions->contains($permission->id)) checked @endif @endif>
            <label for="permission-{{ $permission->name }}">
                {{ ucfirst($permission->name) }}
            </label>
        </div>
    @endforeach
</div>

<div class="form-group">
    <small>(*) Campos obrigatórios</small>
</div>
