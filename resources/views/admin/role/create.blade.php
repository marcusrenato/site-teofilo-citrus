@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/icheck-bootstrap.css') }}">
@endsection

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Cargos
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.role.index') }}">
                        Cargos
                    </a>
                </li>

                <li class="breadcrumb-item active">
                    Adicionar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
@include('sweetalert::alert')

    <div class="card card-outline card-success">
        <div class="card-header">
            <div class="card-title">
                Adicionar novo cargo
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('admin.role.store') }}" method="POST">

                @component('admin.role._partials.form', ['permissions' => $permissions])
                @endcomponent

                <button type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i>
                    Adicionar
                </button>
            </form>
        </div>
    </div>
@endsection
