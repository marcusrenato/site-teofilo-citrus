@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h5>
                Dashboard
            </h5>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">
                    Início
                </li>
            </ol>
        </div>
    </div>
@stop

@section('content')
    
    <h2>
        Bem vindo à área administrativa do seu site.
    </h2>
        
@stop