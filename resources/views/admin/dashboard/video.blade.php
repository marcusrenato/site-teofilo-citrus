@extends('layouts.classroom')

@section('title', 'Dashboard')

@section('content')

    <section class="pagina-curso">
        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Library</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>

        <h2 class="titulo-aula">
            Aula 1 - Introdução ao curso
        </h2>

        <div class="video-wrapper">
            <iframe title="vimeo-player" src="https://player.vimeo.com/video/569849135?h=846a8d2e99" frameborder="0" allowfullscreen></iframe>
            {{-- <video
                alt="video aula"
                class="video-aula"
                src=""
                poster="{{asset('/images/thumbnail/thumbnail.jpg')}}">
            >

            </video>
            <div class="overlay">
                <svg xmlns="http://www.w3.org/2000/svg" width="66" height="66" viewBox="0 0 66 66" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M33 66C51.2254 66 66 51.2254 66 33C66 14.7746 51.2254 0 33 0C14.7746 0 0 14.7746 0 33C0 51.2254 14.7746 66 33 66ZM25.5 20.0096L48 33L25.5 45.9904V20.0096Z" fill="#F5F5F5"/>
                </svg>
                <p>Iniciar Aula</p>
            </div> --}}
        </div>
        <nav class="nav-aulas">
            <a href="#" class="proxima-aula">
                Próxima Aula
                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12" fill="none">
                    <path d="M0 12L8.5 6L0 0V12ZM10 0V12H12V0H10Z" fill="white"/>
                </svg>
            </a>

            <button>
                Marcar como Concluído
                <div class="check"></div>
                <input type="checkbox" name="" id="">
            </button>
        </nav>

        <div class="material-complementar">
            <h3>Material Complementar</h3>
            <ul>
                <li>Slides da Aula <a href="#">link/dbavy.com</a></li>
                <li>Artigo Sobre <a href="#">link/dbavy.com</a></li>
                <li>Sugestão de Leitura <a href="#">link/dbavy.com</a></li>
            </ul>
        </div>
    </section>

@stop

@section('js')
    <script>
        console.log('teste')
    </script>
@endsection
