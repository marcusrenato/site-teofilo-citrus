@extends('adminlte::page')

@section('title', isset($video) ? 'Edição de vídeo' : 'Cadastrar vídeo')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Vídeo
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.video.index') }}">
                        Vídeos
                    </a>
                </li>

                <li class="breadcrumb-item active">
                    Adicionar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-success">
        <div class="card-header">
            <div class="card-title">
                {{ isset($video) ? 'Edição vídeo' : 'Cadastrar vídeo' }}
            </div>
        </div>

        <div class="card-body">
            @if (isset($video))
                <form action="{{ route('admin.video.update', $video->id) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            @else
                <form action="{{ route('admin.video.store') }}" method="POST" enctype="multipart/form-data">
            @endif
                @csrf

                <div class="form-group">
                    <label for="title">Título <span class="text-danger">*</span></label>
                    <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" value="{{ $video->title ?? old('title') }}" maxlength="150" required>

                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                @isset($video->image)
                    <img src="{{ asset('storage/' . $video->image) }}" height="100" width="100" class="img-responsive img-thumbnail">
                @endisset

                <div class="form-group">
                    <label for="image">Imagem para Capa</label>
                    <input type="file" name="image" id="image" class="form-control-file @error('image') is-invalid @enderror" >

                    @error('image')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="category_video_id">
                        Categoria <span class="text-danger">*</span>
                    </label>

                    <select name="category_video_id" id="category_video_id" class="form-control @error('category_video_id') is-invalid @enderror">
                        <option value="">Selecione uma categoria</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" @if(isset($video)) @if($video->category_video_id === $category->id) selected @endif @else @if(old('category_video_id') == $category->id) selected @endif @endif>
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </select>

                    @error('category_video_id')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="type_video">
                        Tipo de vídeo <span class="text-danger">*</span>
                    </label>

                    <select name="type_video" id="type_video" class="form-control @error('type_video') is-invalid @enderror">
                        <option value="">Selecione um tipo de vídeo</option>
                        @foreach ($types as $key => $type)
                            <option value="{{ $key }}" @if(isset($video)) @if($video->type_video === $key) selected @endif @else @if(old('type_video') == $key) selected @endif @endif>
                                {{ ucfirst($type) }}
                            </option>
                        @endforeach
                    </select>

                    @error('type_video')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group" id="form-link" style="display: none">
                    <label for="video">Link do vídeo <span class="text-danger">*</span></label>
                    <input type="text" name="video" id="video" class="form-control @error('video') is-invalid @enderror" value="{{ $video->video ?? old('video') }}">

                    @error('video')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group" id="form-upload" style="display: none">
                    <label for="video_upload">Vídeo <span class="text-danger">*</span></label>
                    <input type="file" name="video_upload" id="video_upload" class="form-control-file @error('video_upload') is-invalid @enderror" >

                    @error('video_upload')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>


                    <div class="form-group">
                    <small>(<span class="text-danger">*</span>) Campos obrigatórios</small>
                </div>

                <button type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i>
                    {{ isset($video) ? 'Atualizar' : 'Cadastrar' }}
                </button>

                <a href="{{ route('admin.video.index') }}" class="btn btn-secondary">
                    <i class="fa fa-times"></i>
                    Cancelar
                </a>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script>
        let typeVideo = document.querySelector('#type_video')

        if (typeVideo.value) {
            changeFormVideo(parseInt(typeVideo.value))
        }

        typeVideo.addEventListener('change', function () {
            let typeVideoValue = parseInt(typeVideo.value)

            changeFormVideo(typeVideoValue);
        })

        function changeFormVideo(typeVideoValue) {
            let formUpload = document.querySelector('#form-upload')
            let formLink = document.querySelector('#form-link')

            const videoYoutube = parseInt('{{ \App\Enums\TypeVideoEnum::VIDEO_YOUTUBE }}');
            const videoVimeo = parseInt('{{ \App\Enums\TypeVideoEnum::VIDEO_VIMEO }}');
            const videoUpload = parseInt('{{ \App\Enums\TypeVideoEnum::VIDEO_UPLOAD }}');

            if (typeVideoValue === videoYoutube) {
                document.querySelector('#video').placeholder = 'Ex: https://youtu.be/Ee0Qh_nIoHw'
                formUpload.style.display = 'none'
                formLink.style.display = 'block'
            }

            if (typeVideoValue === videoVimeo) {
                document.querySelector('#video').placeholder = 'Ex: https://vimeo.com/666109154'
                formUpload.style.display = 'none'
                formLink.style.display = 'block'
            }

            if (typeVideoValue === videoUpload) {
                formLink.style.display = 'none'
                formUpload.style.display = 'block'
            }
        }

    </script>
@endsection
