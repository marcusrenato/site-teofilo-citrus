@extends('adminlte::page')

@section('title', 'Vídeos')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Vídeos
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Vídeos
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-primary">
        <div class="card-header">
            <div class="card-title">
                Lista de Vídeos
            </div>
        </div>

        <div class="row justify-content-end mr-3 mt-3">
            <a href="{{ route('admin.video.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Adicionar
            </a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Capa</th>
                            <th>Categoria</th>
                            <th>Data Postagem</th>
                            <th>Ações</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($videos as $video)
                            <tr>
                                <td>
                                    {{ $video->title }}
                                </td>

                                <td>
                                    <img src="{{ $video->image_link }}" height="50" width="50" class="img-responsive img-thumbnail">
                                </td>

                                <td>
                                    {{ $video->category->name }}
                                </td>

                                <td>
                                    {{ $video->created_at->format('d/m/Y H:i') }}
                                </td>

                                <td>
                                    @can('editar video')
                                        <a href="{{ route('admin.video.edit', $video->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    @endcan

                                    @can('excluir video')
                                        <form action="{{ route('admin.video.destroy', $video->id) }}" class="d-inline" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir?')" data-toggle="tooltip" data-placement="top" title="Excluir">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
