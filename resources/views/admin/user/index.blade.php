@extends('adminlte::page')

@section('title', 'Usuários')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Usuários
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Usuários
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
@include('sweetalert::alert')

    <div class="card card-outline card-primary">
        <div class="card-header">
            <div class="card-title">
                Usuários do sistema
            </div>
        </div>

        <div class="row justify-content-end mr-3 mt-3">
            <a href="{{ route('admin.user.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Adicionar
            </a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Cargo</th>
                            <th>Ações</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>
                                    {{ $user->id }}
                                </td>
                                <td>
                                    {{ $user->name }}
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{ $user->getRoleNames()->first() ?? '' }}
                                </td>
                                <td>
                                    @if (!$user->isSuper())
                                        @can('editar usuário')
                                            <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endcan
                                    
                                        @can('excluir usuário')
                                            @if (auth()->id() !== $user->id)
                                                <form action="{{ route('admin.user.destroy', $user->id) }}" class="d-inline" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir?')" data-toggle="tooltip" data-placement="top" title="Editar">
                                                        <i class="fa fa-trash-alt"></i>
                                                    </button>
                                                </form>
                                            @endif
                                        @endcan
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
