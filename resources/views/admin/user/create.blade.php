@extends('adminlte::page')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Usuários
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.user.index') }}">
                        Usuários
                    </a>
                </li>

                <li class="breadcrumb-item active">
                    Adicionar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
@include('sweetalert::alert')

    <div class="card card-outline card-success">
        <div class="card-header">
            <div class="card-title">
                Adicionar novo usuário
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('admin.user.store') }}" method="POST">

                @component('admin.user._partials.form', ['roles' => $roles])
                @endcomponent

                <button type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i>
                    Adicionar
                </button>
                <a href="{{ route('admin.user.index') }}" class="btn btn-secondary">
                    <i class="fa fa-times"></i>
                    Cancelar
                </a>
            </form>
        </div>
    </div>
@endsection
