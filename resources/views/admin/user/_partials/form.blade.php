@csrf

<div class="form-group">
    <label for="name">Nome *</label>
    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $user->name ?? old('name') }}">

    @error('name')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="email">Email *</label>
    <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ $user->email ?? old('email') }}">

    @error('email')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="password">Senha *</label>
    <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">

    @error('password')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="password_confirmation">Confirme a senha *</label>
    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control @error('password') is-invalid @enderror">

    @error('password')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="role">
        Cargo *
    </label>

    <select name="role" id="role" class="form-control @error('role') is-invalid @enderror">
        <option value="">Selecione um cargo</option>
        @foreach ($roles as $role)
            <option value="{{ $role->id }}" @if(isset($user)) @if($user->getRole() === $role->id) selected @endif @else @if(old('role') == $role->id) selected @endif @endif>
                {{ $role->name }}
            </option>
        @endforeach
    </select>

    @error('role')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
    @enderror
</div>

<div class="form-group">
    <small>(*) Campos obrigatórios</small>
</div>
