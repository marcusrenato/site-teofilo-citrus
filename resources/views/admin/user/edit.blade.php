@extends('adminlte::page')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Usuário
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.user.index') }}">
                        Usuários
                    </a>
                </li>

                <li class="breadcrumb-item active">
                    Editar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-success">
        <div class="card-header">
            <div class="card-title">
                Editar usuário
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('admin.user.update', $user->id) }}" method="POST">
                @method('PUT')

                @component('admin.user._partials.form', ['user' => $user, 'roles' => $roles])
                @endcomponent

                @can('editar usuário')
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        Atualizar
                    </button>
                @endcan
                <a href="{{ route('admin.user.index') }}" class="btn btn-secondary">
                    <i class="fa fa-times"></i>
                    Cancelar
                </a>
            </form>
        </div>
    </div>
@endsection
