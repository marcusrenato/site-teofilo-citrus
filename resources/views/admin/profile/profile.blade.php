@extends('adminlte::page')

@section('title', 'Perfil')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Perfil
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Atualizar perfil
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
@include('sweetalert::alert')

    <div class="card card-outline card-success">
        <div class="card-header">
            <div class="card-title">
                Informações do seu perfil
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('admin.profile.update', $user->id) }}" method="POST">
                @method('PUT')

                @csrf

                <div class="form-group">
                    <label for="name">Nome *</label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $user->name }}">

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email *</label>
                    <input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Senha</label>
                    <input type="password" name="password" id="password" class="form-control">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Confirme a senha</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">

                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <small>(*) Campos obrigatórios</small>
                </div>


                <button type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i>
                    Atualizar
                </button>
            </form>
        </div>
    </div>
@endsection
