@extends('adminlte::page')

@section('title', 'Departamentos')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Departamentos
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Departamentos
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-primary">
        <div class="card-header">
            <div class="card-title">
                Lista de Departamentos
            </div>
        </div>

        <div class="row justify-content-end mr-3 mt-3">
            <a href="{{ route('admin.department.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Adicionar
            </a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Número de Currículos Recebidos</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($departments as $department)
                        <tr>
                            <td>
                                {{ $department->name }}
                            </td>

                            <td>
                                {{ $department->email }}
                            </td>

                            <td>
                                {{ $department->resumes->count() }}
                            </td>

                            <td>
                                @can('editar departamento')
                                    <a href="{{ route('admin.department.edit', $department->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endcan

                                @can('excluir departamento')
                                    <form action="{{ route('admin.department.destroy', $department->id) }}" class="d-inline" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir?')" data-toggle="tooltip" data-placement="top" title="Excluir">
                                            <i class="fa fa-trash-alt"></i>
                                        </button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
