@extends('adminlte::page')

@section('title', isset($category) ? 'Edição departamento' : 'Cadastrar departamaneto')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Departamento
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.department.index') }}">
                        Departamentos
                    </a>
                </li>

                <li class="breadcrumb-item active">
                    Adicionar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-success">
        <div class="card-header">
            <div class="card-title">
                Adicionar novo departamento
            </div>
        </div>

        <div class="card-body">
            @if (isset($department))
                <form action="{{ route('admin.department.update', $department->id) }}" method="POST">
                @method('PUT')
            @else
                <form action="{{ route('admin.department.store') }}" method="POST">
                    @endif

                    @csrf

                    <div class="form-group">
                        <label for="name">Nome <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $department->name ?? old('name') }}" maxlength="255">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail <span class="text-danger">*</span></label>
                        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ $department->email ?? old('email') }}" maxlength="150">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>


                    <div class="form-group">
                        <small>(<span class="text-danger">*</span>) Campos obrigatórios</small>
                    </div>


                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        {{ isset($category) ? 'Atualizar' : 'Cadastrar' }}
                    </button>

                    <a href="{{ route('admin.department.index') }}" class="btn btn-secondary">
                        <i class="fa fa-times"></i>
                        Cancelar
                    </a>
                </form>
        </div>
    </div>
@endsection
