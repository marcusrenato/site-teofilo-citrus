@extends('adminlte::page')

@section('title', isset($category) ? 'Edição de categoria de vídeo' : 'Cadastrar categoria de vídeo')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Categoria de vídeo
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.category_video.index') }}">
                        categorias de vídeo
                    </a>
                </li>

                <li class="breadcrumb-item active">
                    Adicionar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-success">
        <div class="card-header">
            <div class="card-title">
                {{ isset($category) ? 'Edição de categoria de vídeo' : 'Cadastrar categoria de vídeo' }}
            </div>
        </div>

        <div class="card-body">
            @if (isset($category))
                <form action="{{ route('admin.category_video.update', $category->id) }}" method="POST">
                @method('PUT')
            @else
                <form action="{{ route('admin.category_video.store') }}" method="POST">
                    @endif

                    @csrf

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-10">
                                <label for="name">Nome <span class="text-danger">*</span></label>
                                <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $category->name ?? old('name') }}" maxlength="150" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-2">
                                <label for="name">Cor da fonte<span class="text-danger">*</span></label>
                                <input type="color" name="color" id="color" class="form-control @error('color') is-invalid @enderror" value="{{ $category->color ?? old('color') }}" required>

                                @error('color')
                                    <span class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>


                    </div>


                    <div class="form-group">
                        <small>(<span class="text-danger">*</span>) Campos obrigatórios</small>
                    </div>


                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        {{ isset($category) ? 'Atualizar' : 'Cadastrar' }}
                    </button>

                    <a href="{{ route('admin.category_video.index') }}" class="btn btn-secondary">
                        <i class="fa fa-times"></i>
                        Cancelar
                    </a>
                </form>
        </div>
    </div>
@endsection
