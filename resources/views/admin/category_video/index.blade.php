@extends('adminlte::page')

@section('title', 'Categorias de vídeo')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Categorias de vídeo
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Categorias de vídeo
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-primary">
        <div class="card-header">
            <div class="card-title">
                Lista de categorias de vídeo
            </div>
        </div>

        <div class="row justify-content-end mr-3 mt-3">
            <a href="{{ route('admin.category_video.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Adicionar
            </a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Cor da fonte</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>
                                {{ $category->name }}
                            </td>

                            <td>
                                <i class="fa fa-square fa-2x" style="color: {{ $category->color }}"></i>
                            </td>

                            <td>
                                @can('editar categoria de video')
                                    <a href="{{ route('admin.category_video.edit', $category->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endcan

                                @can('excluir categoria de video')
                                    <form action="{{ route('admin.category_video.destroy', $category->id) }}" class="d-inline" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir?')" data-toggle="tooltip" data-placement="top" title="Excluir">
                                            <i class="fa fa-trash-alt"></i>
                                        </button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
