@extends('adminlte::page')

@section('title', 'Currículos')

@section('content_header')
    <div class="row">
        <div class="col-sm-6">
            <h1>
                Currículos
            </h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard.index') }}">
                        Início
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Currículos
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @include('sweetalert::alert')

    <div class="card card-outline card-primary">
        <div class="card-header">
            <div class="card-title">
                Lista de Currículos
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Celular</th>
                        <th>Departamento</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach ($resumes as $resume)
                            <tr>
                                <td>
                                    {{ $resume->name }}
                                </td>

                                <td>
                                    {{ $resume->email }}
                                </td>

                                <td>
                                    {{ $resume->phone_number }}
                                </td>

                                <td>
                                    {{ $resume->department->name }}
                                </td>

                                <td>
                                    <a href="{{ route('admin.resume.download', $resume->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Baixar Currículo">
                                        <i class="fa fa-file-pdf"></i>
                                    </a>
                                    @can('excluir curriculo')
                                        <form action="{{ route('admin.resume.destroy', $resume->id) }}" class="d-inline" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir?')" data-toggle="tooltip" data-placement="top" title="Excluir">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
