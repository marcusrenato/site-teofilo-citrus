{{-- Footer --}}

<footer>
    <div class="container-fluid footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h5>FALE CONOSCO</h5>
                    <h6>(19) 3647-1267</h6>
                    <a class="mail" href="mailto:contato@teofilocitrus.com.br">contato@teofilocitrus.com.br</a>
                    <div class="sociais">
                        <a href="https://www.facebook.com/teofilocitrus/" target="_blank"><i class="fa fa-facebook-square"> Siga-nos no Facebook</i></a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="menu">
                        <a href="{{ route(RouteNames::ROUTE_NAME_INDEX) }}">HOME</a>
                        <a href="{{ route(RouteNames::ROUTE_NAME_COMPANY) }}">EMPRESA</a>
                        <a href="{{ route(RouteNames::ROUTE_NAME_PARTNERSHIP) }}">PARCERIA</a>
                        <a href="{{ route(RouteNames::ROUTE_NAME_PROD_SEEDLINGS) }}">PRODUTOS</a>
                        <a href="{{ route(RouteNames::ROUTE_NAME_VIDEO) }}">VÍDEOS</a>
                        <a href="{{ route(RouteNames::ROUTE_NAME_CAREER) }}">TRABALHE CONOSCO</a>
                        <a href="{{ route(RouteNames::ROUTE_NAME_CONTACT) }}">CONTATO</a>
                    </div>
                    <div class="descricao">
                        <p><strong>Nossa Empresa</strong></p>
                        <p>Localizada na Fazenda São José da Barra, no município de São José do Rio Pardo (SP), suas instalações, Administrativas e de produção, encontram-se em uma área de 1.300 hectares onde, estrategicamente, não há produção de citros.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <a href="https://www.gconci.com.br" target="_blank" class="logo-footer"><img src="/img/logo-footer-1.jpg" class="img-responsive center-block"></a>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <a href="https://www.climatempo.com.br/" target="_blank"><img src="/img/logo-footer-3.jpg" class="img-responsive center-block"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container footer-copy">
        <div class="row">
            <div class="col-md-12">
                <p>All Rights @ {{ date('Y') }} Teófilo Citrus - Special Vegetables</p>
            </div>
        </div>
    </div>
</footer>


@include('partials.script')
