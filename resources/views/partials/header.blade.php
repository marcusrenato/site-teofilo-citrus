{{--Header--}}
<header class="header">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar-principal">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-brand-centered" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1 class="navbar-brand-centered">
                    <a class="logo" href="#" title="Teófilo Citrus - Mudas Cítricas">Teófilo Citrus - Mudas Cítricas</a>
                    <a class="logo-mobile" href="#" title="Teófilo Citrus - Mudas Cítricas">Teófilo Citrus - Mudas Cítricas</a>
                </h1>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-brand-centered">
                <ul class="nav navbar-nav navbar-left">
                    <li class="active"><a href="{{ route(RouteNames::ROUTE_NAME_INDEX) }}">Home</a></li>
                        <li class=" nav dropdown">
                            <a href="{{ route(RouteNames::ROUTE_NAME_COMPANY) }}">Empresa</a>
                        </li>

                    <li><a href="{{ route(RouteNames::ROUTE_NAME_PARTNERSHIP) }}">Parceria</a></li>

                    <li class="dropdown">
                        <a class="">Produtos</a>
                        <div class="dropdown-content">
                            <a href="{{ route('site.seedlings') }}">Mudas Cítricas</a>
                            <a href="{{ route('site.grafts') }}">Porta-Enxertos</a>
                            <a href="{{ route('site.bubbles') }}">Borbulhas</a>
                        </div>
                    </li>

                </ul>

                <ul class="nav navbar-nav navbar-right" style="display: flex" >
                    <li><a href="{{ route('site.career') }}">Trabalhe conosco</a></li>
                    <li><a href="{{ route('site.video') }}">Videos</a></li>

                    <li><a href="{{ route('site.contact') }}">Contato</a></li>
                </ul>
                <!-- <ul class="nav navbar-nav navbar-opa dropdown hidden-xs">
                    <li><a href="{{ route('site.contact') }}">Contato</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right visible-xs">
                    <li><a href="{{ route('site.contact') }}">Contato</a></li>
                </ul> -->
            </div><!-- /.navbar-collapse -->

        </div><!-- /.container-fluid -->
    </nav>

    @if(Route::currentRouteName() === RouteNames::ROUTE_NAME_INDEX)
        <div id="carrousel-banner" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carrousel-banner" data-slide-to="0" class="active"></li>
                <li data-target="#carrousel-banner" data-slide-to="1"></li>
                <li data-target="#carrousel-banner" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background-image: url('/img/banner-1.jpg');">
                    <div class="carousel-caption">
                        <span class="title">Mudas</span>
                        <span class="description">Mudas, produzidas livre de doenças, devido ao seu rígido controle fitossanitário em todas as fases de produção.</span>
                        <a href="{{ route('site.seedlings') }}">Veja Mais</a>
                    </div>
                </div>
                <div class="item" style="background-image: url('/img/banner-2.jpg');">
                    <div class="carousel-caption">
                        <span class="title">Experiência</span>
                        <span class="description">Profissionais expoentes da citricultura nacional e internacional, com experiência em  assessoria e produção de citrus.</span>
                        <a href="{{ route('site.company') }}#profissionais">Veja Mais</a>
                    </div>
                </div>
                <div class="item" style="background-image: url('/img/banner-3.jpg');">
                    <div class="carousel-caption">
                        <span class="title">Estrutura</span>
                        <span class="description">Instalada em uma região isolada, distante a mais de 4km de pomares cítricos, com total controle, adequadas para preservar seus produtos da influência de agentes externos.</span>
                        <a href="{{ route('site.company') }}">Veja Mais</a>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carrousel-banner" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carrousel-banner" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    @else
        <section class="header-title">
            <div class="container">
                <div class="page-title">
                    <h2>
                         {{ $pageName }}
                    </h2>
                </div>
            </div>
        </section>
    @endif

</header>

