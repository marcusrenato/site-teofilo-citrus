<meta property="og:image" content="@yield('seo.image')" />
<meta property="og:description" content="@yield('seo.description', 'Teófilo Citrus - Mudas Cítricas')">

<meta name="keywords" content="Teófilo Citrus, mudas cítricas">
<meta property="og:type" content="website">
<meta property="og:title" content="Teófilo Citrus - Mudas Cítricas">

<meta name="Description" content="Teófilo Citrus - Mudas Cítricas">
<meta property="og:site_name" content="Teófilo Citrus - Mudas Cítricas">
<meta property="og:url" content="@yield('seo.url', Request::url())">
<meta property="og:image" content="/img/logo.png">
<meta property="og:image:type" content="image/pnh">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">

{{-- twitter tags --}}
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="Site Teófilo Citrus">
<meta name="twitter:description" content="@yield('seo.description.twitter', 'Teófilo Citrus - Mudas Cítricas')">
<meta name="twitter:image" content="/img/logo.png">
<meta name="twitter:image:alt" content="@yield('seo.url.twitter', Request::url())">
