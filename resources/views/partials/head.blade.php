<meta charset="utf-8">
<title>
    @yield('title', env('APP_NAME'))
</title>
<meta name="robots" content="index, follow">

<!-- core CSS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="/favicon.png" type="image/png">
<meta name="description" content="Ozório Juvenil">
<meta name="keywords" content="Ozório Juvenil, deputado"/>
<meta name="author" content="">

@include('partials.seo')

@include('partials.style')

<!--[if lt IE 9]>
<![endif]-->
{{--<link rel="shortcut icon" href="images/ico/favicon.ico">--}}
{{--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png') }}">--}}
{{--<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png') }}">--}}
{{--<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png') }}">--}}
{{--<link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png') }}">--}}
