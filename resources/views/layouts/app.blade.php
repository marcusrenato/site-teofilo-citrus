<!DOCTYPE html>
<html lang="pt-br">

<head>
    @include('partials.head')

    @section('seo.image', "/images/logo.png")
</head>
<!--/head-->

<body class="@yield('class-body', 'home')">
    @include('sweetalert::alert')

	@include('partials.header')

    @yield('content')

    @include('partials.footer')
</body>

</html>
