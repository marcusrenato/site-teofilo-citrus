@extends('layouts.app')

@section('class-body', 'page borbulhas')

@section('content')
    <section class="main main-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="title-dark">Conheça nossos Porta-enxertos</h3>
                    <p class="description-dark">As mudas da Teofilo Citrus, garantem ao produtor alta produtividade de frutas e livre de doenças, devido ao seu rígido controle fitossanitário em todas as suas dependências.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="main list-produtos">
        <div class="container-fluid no-gutters">
            <div class="col-md-2 col-lg-2 muda"><img src="/img/produtos/porta-enxertos.jpg" class="img-responsive center-block" /></div>
            <div class="col-md-6 col-lg-5 descricao">
                <h4>Mais qualidade na colheita e no plantio</h4>
                <p>Possuímos estufas específicas, tubetes de 60 ml, com 7 estrias internas e desenvolvimento sobre bandejas, distantes a 1 m altura do piso das estufas, para melhor qualidade fazemos  mais de 3 seleções antes do transplantio.</p>
                <p>Estes são as variedades de porta enxertos produzidas pela Teófilo Citrus:</p>
                <ul>
                    <li>Citrange Carrizo.</li>
                    <li>Citrumelo Swingle.</li>
                    <li>Limão Volkamericano.</li>
                    <li>Poncirus Trifoliata.</li>
                    <li>Flying Dragon.</li>
                    <li>Tangerina Cleópatra.</li>
                    <li>Citrange Troyler.</li>
                    <li>Limão Cravo Santa Bárbara.</li>
                    <li>Laranja Caipira DAC.</li>
                    <li>Poncirus Trifoliata Davis A.</li>
                    <li>Tangelo Orlando.</li>
                    <li>Tangerina Sunki.</li>
                </ul>
            </div>
            <div class="col-md-4 col-lg-5 img-porta-enxertos"><img src="/img/produtos/2.jpg" class="img-responsive" /></div>
        </div>
    </section>
@endsection
