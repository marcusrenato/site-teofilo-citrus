@extends('layouts.app')

@section('title', 'Teófilo Cetris')

@section('class-body', 'page empresa')

@section('styles')
    <link href="/js/owl/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/js/owl/assets/owl.theme.default.min.css" rel="stylesheet">
@endsection

@section('content')
    <section class="main apresentacao">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="title">Apresentação</h3>
                    <p>Fundada em 1998, a Teófilo Citrus é uma empresa, dedicada ao sucesso de seus clientes, através da troca de informações e a produção de mudas cítricas de altíssima qualidade genética e comprovada produtividade.</p>
                    <p>Nossa organização possui uma equipe de colaboradores capacitados e comprometidos com os nossos parceiros, visando não apenas atender suas necessidades, mas sim supera-las.</p>
                    <p>Passamos por constantes atualizações para atender a todas as normas fitossanitárias, visando entregar a nossos parceiros produtos e serviços com qualidade e procedência, que agreguem valor a sua plantação.</p>

                    <h3 class="title">Nossa Missão</h3>
                    <p>A Teófilo Citrus tem por objetivo fornecer produtos e serviços no mercado de ctirus que atendam às necessidades e expectativas de nossos parceiros, atendendo todas as normas fitossanitárias que regem sua atividade e contribuindo para o incremento do setor do agronegócio e o crescimento da Sociedade e a preservação do Meio Ambiente.</p>

                    <h3 class="title">Nossa Visão</h3>
                    <p>Destacar-se por nossa qualidade em seus produtos e serviços, sendo referência no mercado por sua confiabilidade e excelência, agregando valor a nossos parceiros, oferecendo produtos que atendam e superem suas necessidades e expectativas.</p>

                    <h3 class="title">Nossos Valores</h3>
                    <ul>
                        <li> Ética</li>
                        <li> Transparência</li>
                        <li> Honestidade</li>
                        <li> Confiança</li>
                        <li> Parcialidade</li>
                        <li> Agilidade</li>
                        <li> Respeito as pessoas e ao Meio Ambiente</li>
                        <li> Competência</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="main estrutura">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="title" id="nossa-estrutura">Nossa Estrutura</h3>
                    <p>Localizada na Fazenda São José da Barra, no município de São José do Rio Pardo (SP), suas instalações, Administrativa e de produção, encontram-se em uma área de 1.300 hectares onde, estrategicamente, não há produção de citros e com mais de 4 km de distancia de pomares comerciais.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="estrutura-slider">
        <div class="owl-carousel owl-theme">
            <div class="item"><img src="/img/slider-empresa/1.jpg" /></div>
            <div class="item"><img src="/img/slider-empresa/2.jpg" /></div>
            <div class="item"><img src="/img/slider-empresa/3.jpg" /></div>
            <div class="item"><img src="/img/slider-empresa/4.jpg" /></div>
            <div class="item"><img src="/img/slider-empresa/5.jpg" /></div>
            <div class="item"><img src="/img/slider-empresa/6.jpg" /></div>
            <div class="item"><img src="/img/slider-empresa/7.jpg" /></div>
            <div class="item"><img src="/img/slider-empresa/8.jpg" /></div>
        </div>
    </section>

    <section class="main estrutura-icons">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/estufas.png" class="center-block" />
                    <p>Ambientes protegidos com estufas teladas.</p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/localizacao.png" class="center-block" />
                    <p>Localização isolada: os viveiros estão distantes de pomares de citros.</p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/poco.png" class="center-block" />
                    <p>Água proveniente de poço semi-artesiano.</p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/camara-fria.png" class="center-block" />
                    <p>Câmara Fria: responsável pela preservação das características e potencialidades vitais das sementes.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <img src="/img/icones-empresa/lab.png" class="center-block" />
                    <p>Laboratório próprio para análises químicas e fitopatológicas.</p>
                </div>
                <div class="col-sm-6 col-md-4">
                    <img src="/img/icones-empresa/ambientes-controlados.png" class="center-block" />
                    <p>Ambientes controlados em estufas específicas: Borbulhas, Porta-enxertos e Mudas.</p>
                </div>
                <div class="col-sm-6 col-md-4">
                    <img src="/img/icones-empresa/vestiarios.png" class="center-block" />
                    <p>Vestiários e refeitórios: representações concretas do respeito dispensado aos nossos colaboradores, que refletem os valores que norteiam a empresa.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="main profissionais">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-dark" id="profissionais">Profissionais</h3>
                    <div class="row">
                        <div class="col-md-6 profissional col-sm-offset-1">
                            <p><strong>José Eduardo M. Teófilo</strong></p>
                            <p>Engenheiro Agrônomo pela UFLA - Universidade Federal de Lavras, com Pós-Graduação em Adm e Gestão, atua como consultor em citros deste 1990. Teófilo é membro fundador do Grupo de Consultores em Citros (GCONCI), e do Conselho Editorial da Revista Citricultura Atual.</p>
                        </div>
                        <div class="col-md-2"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="/js/owl/owl.carousel.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('.owl-carousel').owlCarousel({
                margin: 0,
                loop: true,
                nav: true,
                items: 4,
                responsiveClass: true,
                navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 2,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false
                    }
                }
            });
        });
    </script>
@endsection
