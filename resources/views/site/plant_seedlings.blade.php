@extends('layouts.app')

@section('class-body', 'page mudas')

@section('content')
    <section class="main main-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="title-dark">Mudas Cítricas</h3>
                    <p class="description-dark">A Muda é o mais importante investimento do citricultor, na medida em que os citros são culturas perenes. Enquanto os procedimentos técnicos e operacionais podem ser gerenciados, modificados ou adaptados, as mudas - o princípio vital da plantação - serão as mesmas. Portanto, da qualidade e adequação das mudas dependerá, em grande parte, o sucesso do empreendimento.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="main list-produtos">
        <div class="container-fluid no-gutters">
            <div class="col-md-4 col-lg-5 img-citrica"><img src="/img/produtos/1.jpg" class="img-responsive" /></div>
            <div class="col-md-6 col-lg-5 descricao">
                <h4>A Qualidade final das frutas são reflexo do cuidado da produção das mudas.</h4>
                <p>As mudas da Teófilo Citrus, garantem ao produtor alta produtividade de frutas e livre de doenças, devido ao seu rígido controle fitossanitário em todas as suas dependências:</p>
                <ul>
                    <li>Baia Cabula</li>
                    <li>Baianinha</li>
                    <li>Folha Murcha</li>
                    <li>Hamilin</li>
                    <li>Lima Sorocaba</li>
                    <li>Lima Verde</li>
                    <li>Murcote</li>
                    <li>Natal</li>
                    <li>Pêra Bianchi</li>
                    <li>Pêra IAC</li>
                    <li>Pêra Ipiguá</li>
                    <li>Pêra Olímpia</li>
                    <li>Pomelo Star Rubi</li>
                    <li>Poncã</li>
                    <li>Rubi</li>
                    <li>Tahiti IAC – 5</li>
                    <li>Valência</li>
                    <li>Westin</li>
                    <li>Charmute</li>
                    <li>Pêra rio</li>
                    <li>IAC 2000</li>
                    <li>Valência Americana</li>
                </ul>
            </div>
            <div class="col-md-2 col-lg-2 muda"><img src="/img/produtos/muda-citrica.jpg" class="img-responsive center-block" /></div>
        </div>
    </section>

@endsection
