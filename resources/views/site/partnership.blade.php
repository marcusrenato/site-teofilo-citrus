@extends('layouts.app')

@section('class-body', 'page motivos')

@section('content')
    <section class="main apresentacao">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="title">7 motivos para você ser um parceiro da Teófilo Citrus</h3>
                    <p>Atendemos uma ampla região do Brasil, oferecendo Mudas Cítricas, Borbulhas, Porta-Enxertos e Serviços de Consultoria especializada.</p>


                    <p>Prezamos por um atendimento de qualidade, em nossos produtos e serviços. Para a Teófilo Citrus é essencial que as expectativas e necessidades de nossos clientes não sejam apenas cumpridas, mas também superadas.</p>


                    <p>24 anos produzindo mudas de qualidade e excelência. | Faça-nos uma visita e conheça nossa estrutura.</p>


                </div>
            </div>
        </div>
    </section>

    <section class="main estrutura-icons">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/um.png" class="center-block" />
                    <p><b>PROCEDÊNCIA</b></p>
                    <p>Contamos com um rigoroso controle fitossanitário, trabalhando em parceria com os órgãos competentes afim de cumprir todos os protocolos necessários para a boa sanidade de nossos produtos e processos.</p>
                </div>

                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/dois.png" class="center-block" />
                    <p><b>QUALIDADE</b></p>
                    <p>Trabalhamos somente com sementes certificadas e rigorosamente selecionadas, analisadas quimicamente em laboratório, gerando mudas saudáveis e livres de doenças, garantindo a nossos parceiros uma alta produtividade de frutos.</p>
                </div>

                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/tres.png" class="center-block" />
                    <p><b>CAPACITAÇÃO</b></p>
                    <p>Contamos com uma equipe de profissionais capacitados, referenciados no mercado de citros, os quais estão em constante treinamento para a aplicação de novas tecnologias para melhoria continua de nossos produtos e processos.</p>
                </div>

                <div class="col-sm-6 col-md-3">
                    <img src="/img/icones-empresa/quatro.png" class="center-block" />
                    <p><b>LOCALIZAÇÃO</b></p>
                    <p>Estamos estabelecidos em São José do Rio Pardo, estado de São Paulo, em uma área distante de pomares de citrus, visando controlar nosso sistema produtivo com maior eficácia.</p>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <img src="/img/icones-empresa/cinco.png" class="center-block" />
                    <p><b>CONFIANÇA</b></p>
                    <p>Todos os produtos utilizados em nossas mudas possuem origem em fornecedores qualificados e registrados nos órgãos competentes, garantindo assim, insumos de qualidade e procedência para nosso sistema produtivo.</p>
                </div>


                <div class="col-sm-6 col-md-4">
                    <img src="/img/icones-empresa/seis.png" class="center-block" />
                    <p><b>FEEDBACK</b></p>
                    <p>Por meio dos nossos canais de comunicação (Telefone, site, facebook, instagram e whats app), estamos sempre prontos para atender nossos parceiros em suas necessidades e expectativas.</p>
                </div>

                <div class="col-sm-6 col-md-4">
                    <img src="/img/icones-empresa/sete.png" class="center-block" />
                    <p><b>RESPEITO</b></p>
                    <p>Prezamos sempre pelo bom relacionamento com nossos parceiros e fornecedores, através de valores e princípios que regem nossa organização, visando manter uma base sólida de parceria e um relacionamento duradouro de benefício mútuo.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="main profissionais">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-dark" id="profissionais">Experiência</h3>
                    <div class="row">
                        <div class="col-md-6 profissional col-sm-offset-1">
                            <p><strong>know-how</strong></p>
                            <p>Nossa experiência na citricultura proporciona um know-how com padrão diferenciado para auxiliar o produtor
                                no controle fitossanitario e na nutrição correta das mudas para ter uma maior produtividade e melhor qualidade dos frutos. Nossa assessoria vai desde a plantação das mudas até a colheita final dos frutos. </p>




                        </div>
                        <div class="col-md-2"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
