@extends('layouts.app')

@section('class-body', 'page contato')

@section('content')

    <section class="main main-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="title-dark">Fale Conosco</h3>
                    <p class="description-dark">•	Nós, da Teófilo Citrus queremos conhecê-lo, esclarecer suas dúvidas e receber suas sugestões em relação aos nossos serviços.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="main formulario">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <form id="form-contato" method="POST" action="/scripts/enviar_contato.php">
                        <input hidden="sendform" value="true" />
                        <div class="form-group">
                            <input type="text" class="form-control" id="nome" required name="nome" placeholder="Nome:" />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" required name="email" placeholder="E-mail:" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="telefone" required name="telefone" placeholder="Telefone:" />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="7" id="mensagem" required name="mensagem" placeholder="Mensagem:"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default enviar-form">Enviar</button>
                    </form>
                </div>
                <div class="col-md-6 contatos">
                    <div class="row">
                        <div class="col-xs-3"><img src="/img/contato/icon-administracao.png" class="center-block" /></div>
                        <div class="col-xs-9">
                            <h4>Gerência</h4>
                            <a href="mailto:teofilo@teofilocitrus.com.br">gerencia@teofilocitrus.com.br</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3"><img src="/img/contato/icon-financeiro.png" class="center-block" /></div>
                        <div class="col-xs-9">
                            <h4>Financeiro</h4>
                            <a href="mailto:financeiro@teofilocitrus.com.br">financeiro@teofilocitrus.com.br</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3"><img src="/img/contato/icon-rh.png" class="center-block" /></div>
                        <div class="col-xs-9">
                            <h4>Recursos Humanos</h4>
                            <a href="mailto:rh@teofilocitrus.com.br">rh@teofilocitrus.com.br</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3"><img src="/img/contato/icon-compras.png" class="center-block" /></div>
                        <div class="col-xs-9">
                            <h4>Compras</h4>
                            <a href="mailto:compras@teofilocitrus.com.br">compras@teofilocitrus.com.br</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3"><img src="/img/contato/icon-vendas.png" class="center-block" /></div>
                        <div class="col-xs-9">
                            <h4>Vendas</h4>
                            <a href="mailto:vendas@teofilocitrus.com.br">vendas@teofilocitrus.com.br</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3"><img src="/img/contato/icon-tecnica.png" class="center-block" /></div>
                        <div class="col-xs-9">
                            <h4>Análise</h4>
                            <a href="mailto:analise@teofilocitrus.com.br">analise@teofilocitrus.com.br</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main localizacao">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="title">Localização</h3>
                    <p> Teófilo Citrus - está instalada na Fazenda São José da Barra, no município de São José do Rio pardo/SP</p>
                </div>
            </div>
        </div>
    </section>
    <section class="main mapa">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d99761.14169823333!2d-47.05955519266325!3d-21.687356282365766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b7d1d9ec024e2d%3A0xe43e0bf95f3dfda5!2sTe%C3%B3filo%20Citrus%20Mudas%20C%C3%ADtricas!5e0!3m2!1sen!2sbr!4v1642132321813!5m2!1sen!2sbr" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </section>

@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-EdLSWLCk8xqKn63JjM4ls9pzVx2sugI&callback=initMap"></script>
    <script src="/js/map.js"></script>
@endsection
