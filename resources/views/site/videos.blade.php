@extends('layouts.app')

@section('class-body', 'page videos')

@section('content')
    <section class="main destaques">

        <div class="container">
            <h3>ÚLTIMOS DESTAQUES</h3>
            <div class="grid">

                @foreach($videos as $video)
                    <div class="card-video {{ $video->class_player }}" @if($video->is_upload) data-video-url="{{ asset('storage/' . $video->video) }}" @else data-video-id="{{ $video->video_id }}" @endif>
                        <div class="img">
                            <img src="{{ $video->image_link }}">
                            <div class="play">
                                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="play" class="svg-inline--fa fa-play fa-w-14" role="img" viewBox="0 0 448 512"><path fill="#ffffff" d="M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z"/></svg>
                            </div>
                        </div>
                        <div class="description">
                            <h5 class="title">{{ $video->title }}</h5>
                            <p class="subject yellow" style="color: {{ $video->category->color }}">{{ strtoupper($video->category->name) }}</p>
                            <small class="posted">Postado em {{ $video->created_at->format('d/m/Y') }} </small>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="main fale-conosco">
        <div class="container">
            <h2>Agende seu horário</h2>
            <p>Se você tem interesse em agendar uma entrevista ou conhecer um pouco mais das nossas instalações, agende um horário conosco por meio do botão abaixo.</p>
            <a href="/contato">FALE CONOSCO</a>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        new ModalVideo('.js-modal-btn');
        new ModalVideo('.js-modal-btn-vimeo', {channel: 'vimeo'});
    </script>
@endsection
