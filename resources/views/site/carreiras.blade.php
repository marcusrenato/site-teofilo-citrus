@extends('layouts.app')

@section('title', 'Carreiras - Teófilo Cetris')

@section('class-body', 'page carreiras')

@section('content')
    @include('sweetalert::alert')
    <section class="main apresentacao">
        <div class="container">
            <div class="row">
                <div class="form">
                    <h5 class="sub-title">Envie seu currículo</h5>
                    <form action="{{ route('site.career-send') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-lg-12">
                                <input placeholder="Nome:" type="text" name="name" required>
                            </div>

                            <div class="col-lg-6">
                                <input placeholder="Fone:" type="text" name="phone_number" required>
                            </div>

                            <div class="col-lg-6">
                                <input placeholder="E-mail:" type="email" name="email" required>
                            </div>

                            <div class="col-lg-12">
                                <select name="department_id" id="">
                                    <option value="" disabled selected hidden>Área de Interesse:</option>
                                    @foreach($departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-lg-12">
                                <input type="file" name="resume" id="upload-photo" required>
                                <label for="upload-photo">
                                    <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" version="1.2" baseProfile="tiny-ps" viewBox="0 0 414 451" height="20">
                                            <title>Camada 1</title>
                                            <style>
                                                tspan { white-space:pre }
                                                .s0 { fill: #ffffff }
                                            </style>
                                            <g id="Camada 1">
                                                <g id="jqLs0O.tif">
                                                    <g id="&lt;Group&gt;">
                                                        <path id="&lt;Path&gt;" class="s0" d="m125.99 0.48c6.67 1.65 13.43 3 19.98 5.02c17.14 5.29 32.31 14.15 45.01 26.82c59.22 59.09 118.27 118.35 177.53 177.39c5.83 5.8 6.82 11.02 1.22 17.16c-2.47 2.7-4.78 5.58-7.52 7.99c-9.3 8.21-13 8.07-21.8-0.73c-57.73-57.75-115.38-115.58-173.17-173.27c-26.65-26.6-63.25-31.69-93.01-13.31c-42.27 26.09-48.28 82.88-12.14 119.5c43.78 44.36 88.03 88.25 132.08 132.34c31.33 31.36 62.64 62.74 94.01 94.06c10.63 10.62 22.84 17.75 38.34 19.3c36.36 3.64 58.45-29.13 45.87-61.98c-3.23-8.45-8.51-16.83-14.86-23.25c-56.24-56.88-112.94-113.3-169.47-169.9c-7.06-7.07-15.76-11.96-25.04-7.96c-5.83 2.52-11.62 9.14-13.76 15.21c-3.27 9.29 2.86 17.01 9.49 23.63c32.8 32.72 65.54 65.5 98.3 98.25c4.48 4.48 9.5 8.55 13.27 13.55c3.48 4.62 9.25 12.62 7.7 14.99c-5.04 7.7-12.7 13.85-20.08 19.68c-1.28 1.01-6.87-1.68-9.17-3.97c-41.1-40.9-82.46-81.55-122.77-123.22c-21.53-22.26-19.68-58.99 2.05-80.48c21.81-21.58 58.46-23.04 80.49-1.31c60.75 59.91 121.31 120.03 180.86 181.13c32.5 33.34 34.17 97.33-8.66 127.19c-10.78 7.51-24.43 10.9-36.76 16.18c-8 0-16 0-24 0c-19.53-4.56-38.18-11.11-52.76-25.66c-78.94-78.77-158.3-157.12-236.45-236.66c-45.98-46.79-37.93-126.23 14.59-165.63c16.38-12.28 34.99-18.24 54.62-22.06c8.67 0 17.33 0 26 0z"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        ANEXAR
                                    </div>
                                </label>
                                <small class="form-text text-success text-muted" id="info_arq_done" style="display: none"></small>
                            </div>
                        </div>
                        <button type="submit">ENVIAR AGORA</button>
                    </form>
                </div>

            </div>
        </div>
        <div class="overflow-col">
            <div class="text">
                <h3>
                    FAÇA PARTE <br/>
                    DO NOSSO TIME!
                </h3>

                <p>
                    Queremos conhecer você e as expectativas em contribuir para o desenvolvimento da TS Citrus, para isto, preencha os dados ao lado e anexe seu currículo.
                </p>
            </div>
            <a href="{{ route(\App\Enums\RouteNameEnum::ROUTE_NAME_COMPANY) }}#nossa-estrutura">
                <div class="img">
                    <h3>
                        CONHEÇA <br/>
                        NOSSA ESTRUTURA
                    </h3>
                </div>
            </a>
        </div>
    </section>

    <section class="main map">
        <h3>Onde estamos?</h3>
        <p>A TS Citrus - special vegetables está instalada na Fazenda São José da Barra, no município de Itobi, região de São José do Rio pardo/SP</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d99761.14169823333!2d-47.05955519266325!3d-21.687356282365766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b7d1d9ec024e2d%3A0xe43e0bf95f3dfda5!2sTe%C3%B3filo%20Citrus%20Mudas%20C%C3%ADtricas!5e0!3m2!1sen!2sbr!4v1642132321813!5m2!1sen!2sbr" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </section>

@endsection

@section('scripts')
    <script>
        let inputUpload = document.querySelector('#upload-photo');

        inputUpload.addEventListener('change', function () {
            let textFileOk =  document.querySelector('#info_arq_done')

            let fileName = inputUpload.value.split('\\').pop()

            textFileOk.innerHTML = `O arquivo <b>${fileName}</b> foi anexado <i class="fa fa-check 2x text-success"></i>`
            textFileOk.style.display = 'block'
        })
    </script>
@endsection
