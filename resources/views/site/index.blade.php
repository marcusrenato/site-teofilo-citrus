@extends('layouts.app')

@section('content')
    <section class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h3 class="title">Experiência que faz a diferença</h3>
                    <p class="description">O Processo de produção das borbulhas da Teófilo Citrus inicia-se na seleção das melhores matrizes certificadas, provenientes de uma das mais respeitáveis instituições da citricultura: O Centro APTA Sylvio Moreira e EMBRAPA. As Borbulheiras são mantidas em ambientes telados com para preservação da sanidade e qualidade, com um rígido controle varietal (preservando as características de cada variedade).</p>
                </div>
            </div>
        </div>
    </section>

    <section class="destaques">
        <div class="container-fluid no-gutters">
            <div class="col-xs-12 col-md-4 bloco-destaque bloco-qualidade">
                <div class="img-destaque">
                    <img src="/img/destaque-qualidade.jpg" class="img-responsive" />
                </div>
                <div class="descricao">
                    <h4>Qualidade</h4>
                    <p>As mudas da Teófilo Citrus, garantem ao produtor alta produtividade de frutas e livre de doenças, devido ao seu rígido controle fitossanitário em todas as suas fases e dependências.</p>
                    <a href="{{ route('site.seedlings') }}">Veja</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 bloco-destaque bloco-estrutura">
                <div class="descricao">
                    <h4 class="estrutura">Estrutura</h4>
                    <p>A Teófilo Citrus utiliza toda sua tecnologia disponível e instalações adequadas para proteger  seus produtos da influência de agentes externos, que possam comprometer o vigor e saúde de suas mudas, dentro das normas da Secretaria de Agricultura do Estado de São Paulo e do Ministério da Agricultura.</p>
                    <a href="{{ route('site.company') }}">Veja</a>
                </div>
                <div class="img-destaque">
                    <img src="/img/destaque-estrutura.jpg" class="img-responsive" />
                </div>
            </div>
            <div class="col-xs-12 col-md-4 bloco-destaque bloco-profissionais">
                <div class="img-destaque">
                    <img src="/img/destaque-profissionais.jpg" class="img-responsive" />
                </div>
                <div class="descricao">
                    <h4 class="profissionais">Profissionais</h4>
                    <p>Liderada por profissionais que agregam, às suas próprias formações e experiências profissionais, a convivência com o exemplo e a referência de seu pai, Dr. Joaquim Teófilo Sobrinho, e referência da citricultura nacional e internacional.</p>
                    <a class="ancora" href="{{ route('site.company') }}#profissionais">Veja</a>
                </div>
            </div>

        </div>
    </section>
@endsection
