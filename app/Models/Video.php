<?php

namespace App\Models;

use App\Enums\TypeVideoEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Video extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'type_video',
        'category_video_id',
        'video',
        'order',
        'image'
    ];

    protected $appends = [
        'class_player',
        'is_upload',
        'video_id',
        'image_link'
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(CategoryVideo::class, 'category_video_id');
    }

    public function getClassPlayerAttribute(): string
    {
        if ($this->type_video === TypeVideoEnum::VIDEO_YOUTUBE || $this->type_video === TypeVideoEnum::VIDEO_UPLOAD) {
            return 'js-modal-btn';
        }

        if ($this->type_video === TypeVideoEnum::VIDEO_VIMEO) {
            return 'js-modal-btn-vimeo';
        }

        return '';
    }

    public function getIsUploadAttribute(): bool
    {
        return TypeVideoEnum::ARRAY_TYPE_VIDEOS[$this->type_video] === TypeVideoEnum::VIDEO_UPLOAD_STRING;
    }

    public function getVideoIdAttribute(): string
    {
        if ($this->type_video === TypeVideoEnum::VIDEO_YOUTUBE) {
            $url = explode('/', $this->video);

            return (string) end($url);
        }

        if ($this->type_video === TypeVideoEnum::VIDEO_VIMEO) {
            return (string) filter_var($this->video, FILTER_SANITIZE_NUMBER_INT);
        }

        return '';
    }

    public function getImageLinkAttribute(): string
    {
        return $this->image ? asset('storage/' . $this->image) : asset('/img/bg-title-header-produtos.jpg');
    }
}
