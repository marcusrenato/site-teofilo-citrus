<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Resume extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'department_id',
        'email',
        'phone_number',
        'resume'
    ];

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }
}
