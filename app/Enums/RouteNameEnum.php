<?php

namespace App\Enums;

class RouteNameEnum
{
    public const ROUTE_NAME_INDEX = 'site.index';
    public const ROUTE_NAME_COMPANY = 'site.company';
    public const ROUTE_NAME_PARTNERSHIP = 'site.partnership';
    public const ROUTE_NAME_PROD_SEEDLINGS = 'site.seedlings';
    public const ROUTE_NAME_PROD_GRAFTS = 'site.grafts';
    public const ROUTE_NAME_PROD_BUBBLES = 'site.bubbles';
    public const ROUTE_NAME_CAREER = 'site.career';
    public const ROUTE_NAME_VIDEO = 'site.video';
    public const ROUTE_NAME_CONTACT = 'site.contact';
}
