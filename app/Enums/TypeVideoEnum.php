<?php

declare(strict_types=1);

namespace App\Enums;

class TypeVideoEnum
{
    public const VIDEO_YOUTUBE  = 1;
    public const VIDEO_VIMEO    = 2;
    public const VIDEO_UPLOAD   = 3;

    public const VIDEO_YOUTUBE_STRING  = 'youtube';
    public const VIDEO_VIMEO_STRING    = 'vimeo';
    public const VIDEO_UPLOAD_STRING   = 'upload';

    public const ARRAY_TYPE_VIDEOS = [
        self::VIDEO_YOUTUBE => self::VIDEO_YOUTUBE_STRING,
        self::VIDEO_VIMEO   => self::VIDEO_VIMEO_STRING,
        self::VIDEO_UPLOAD  => self::VIDEO_UPLOAD_STRING
    ];
}
