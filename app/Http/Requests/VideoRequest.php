<?php

namespace App\Http\Requests;

use App\Enums\TypeVideoEnum;
use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $data = $this->all();

        $rules = [
            'title' => 'required|string|min:3|max:50',
            'type_video' => 'required|numeric',
            'category_video_id' => 'required|numeric',
            'image' => 'nullable|image',
        ];

        if ($data['type_video'] == TypeVideoEnum::VIDEO_UPLOAD) {
            $rules['video_upload'] = 'required|file|mimetypes:video/mp4';

            if (request()->isMethod('PUT')) {
                $rules['video_upload'] = 'nullable|file|mimetypes:video/mp4';
            }

        }

        if ($data['type_video'] == TypeVideoEnum::VIDEO_YOUTUBE || $data['type_video'] == TypeVideoEnum::VIDEO_VIMEO) {
            $rules['video'] = 'required|url';
        }

        return $rules;
    }
}
