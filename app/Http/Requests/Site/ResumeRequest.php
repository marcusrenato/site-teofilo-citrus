<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class ResumeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:3|max:150',
            'email' => 'required|string|email',
            'department_id' => 'required|numeric',
            'phone_number' => 'required|max:25',
            'resume' => 'required|mimes:jpg,jpeg,png,pdf,docx,doc'
        ];
    }
}
