<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(4);

        $rules = [
            'name' => 'required|min:3|max:100|string',
            'email' => "required|email|string|unique:users,email,{$id},id",
            'password' => 'required|confirmed|min:6|max:30',
            'role' => 'required'
        ];

        if (request()->isMethod('PUT')) {
            $rules['password'] = 'nullable|string|min:6|max:30';
        }

        return $rules;
    }
}
