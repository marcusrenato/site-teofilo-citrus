<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    protected Role $role;
    protected Permission $permission;

    /**
     * Create a new controller instance
     */
    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function index(): View
    {
        $roles = $this->role->where('id', '<>', 1)->get();

        return view('admin.role.index', compact('roles'));
    }

    public function create(): View
    {
        if (!auth()->user()->isSuper()) {
            $permissions = auth()->user()->role()->first()->permissions;
        } else {
            $permissions = $this->permission->orderBy('id')->get();
        }

        return view('admin.role.create', [
            'permissions' => $permissions
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $data = $request->validate([
            'name' => 'required|max:255|unique:roles,name',
            'permissions' => 'nullable|array'
        ]);

        try {
            /** @var Role $role */
            $role = $this->role->create(['name' => $data['name']]);

            $role->syncPermissions($data['permissions']);

            Alert('Sucesso!', 'Adicionado com sucesso!', 'success');
            return redirect()->route('admin.role.index');
        } catch (\Throwable $th) {

            Log::error(
                'Erro ao inserir cargo', [
                    'reason' => $th->getMessage()
                ]
            );

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    /**
     * @param int $id
     * @return View|RedirectResponse
     */
    public function edit(int $id)
    {
        /** @var User $user */
        $user = auth()->user();

        if ($id === 1) {
            abort(404);
        }

        $role = $this->role->findOrFail($id);

        if (!$user->isSuper()) {

            if ($user->getRole() === null) {
                Alert('info', 'Você não possui permissões para delegar', 'info');
                return redirect()->back();
            }

            $rolePermissions = $this->role->find($user->getRole());

            $permissions = $rolePermissions->getAllPermissions();
        } else {
            $permissions = $this->permission->orderBy('id')->get();
        }

        return view('admin.role.edit', compact('role', 'permissions'));
    }

    public function update(Request $request, int $id): RedirectResponse
    {
        $data = $request->all();

        try {
            $role = $this->role->findOrFail($id);

            $role->update(['name' => $data['name']]);

            if (isset($data['permissions'])) {
                $role->syncPermissions($data['permissions']);
            }

            Alert('Sucesso!', 'Atualizado com sucesso!', 'success');
            return redirect()->route('admin.role.index');
        } catch (\Throwable $th) {

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function destroy(int $id): RedirectResponse
    {
        try {
            $role = $this->role->findOrFail($id);

            $role->delete();

            Alert('Sucesso!', 'Excluído com sucesso!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }
}
