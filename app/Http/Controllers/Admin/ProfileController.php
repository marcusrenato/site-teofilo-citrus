<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProfileController extends Controller
{
    public function index(): View
    {
        $user = auth()->user();

        return view('admin.profile.profile', [
            'user' => $user
        ]);
    }

    public function update(UserRequest $request): RedirectResponse
    {
        $data = $request->all();

        try {
            if (isset($data['password'])) {
                $data['password'] = bcrypt($data['password']);
            } else {
                unset($data['password']);
            }

            /** @var User $user */
            $user = auth()->user();

            $user->update($data);

            Alert('Sucesso', 'Perfil atualizado com sucesso!', 'success');

            return redirect()->back();
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro ao tentar atualizar perfil.', 'error');

            return redirect()->back()->withInput();
        }
    }
}
