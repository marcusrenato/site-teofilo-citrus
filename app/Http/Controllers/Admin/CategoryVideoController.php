<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryVideoRequest;
use App\Models\CategoryVideo;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CategoryVideoController extends Controller
{
    private CategoryVideo $categoryVideo;

    public function __construct(CategoryVideo $categoryVideo)
    {
        $this->categoryVideo = $categoryVideo;
    }

    public function index(): View
    {
        return view('admin.category_video.index', [
            'categories' => $this->categoryVideo->orderBy('id', 'DESC')->get()
        ]);
    }

    public function create(): View
    {
        return view('admin.category_video.form');
    }

    public function store(CategoryVideoRequest $request): RedirectResponse
    {
        $data = $request->all();

        try {
            $this->categoryVideo->create($data);

            Alert('Sucesso!', 'Adicionado com sucesso!', 'success');
            return redirect()->route('admin.category_video.index');
        } catch (\Throwable $th) {

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function edit(CategoryVideo $categoryVideo): View
    {
        return view('admin.category_video.form', [
            'category' => $categoryVideo->select('id', 'name', 'color')->first()
        ]);
    }

    public function update(CategoryVideoRequest $request, CategoryVideo $categoryVideo): RedirectResponse
    {
        $data = $request->all();

        try {
            $categoryVideo->update($data);

            Alert('Sucesso!', 'Atualizado com sucesso!', 'success');
            return redirect()->route('admin.category_video.index');
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function destroy(CategoryVideo $categoryVideo): RedirectResponse
    {
        try {
            $categoryVideo->delete();

            Alert('Sucesso!', 'Excluído com sucesso!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }
}
