<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected User $user;
    protected Role $role;

    /**
     * Create a new controller instance
     */
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    public function index(): View
    {
        $users = $this->user->all();

        return view('admin.user.index', compact('users'));
    }

    public function create(): View
    {
        $roles = $this->role
            ->where('id', '<>', 1)
            ->get();

        return view('admin.user.create', [
            'roles' => $roles
        ]);
    }

    public function store(UserRequest $request): RedirectResponse
    {
        $data = $request->all();

        try {
            $user = $this->user->create($data);

            if (isset($data['role'])) {
                $user->assignRole($data['role']);
            }

            Alert('Sucesso!', 'Adicionado com sucesso!', 'success');
            return redirect()->route('admin.user.index');
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function edit(int $id): View
    {
        $user = $this->user->findOrFail($id);
        $roles = $this->role
            ->where('id', '<>', 1)
            ->get();

        return view('admin.user.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function update(UserRequest $request, int $id): RedirectResponse
    {
        $data = $request->all();

        if ($request->password) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        try {
            $user = $this->user->findOrFail($id);

            $user->update($data);

            if (isset($data['role'])) {
                $user->assignRole($data['role']);
            }

            Alert('Sucesso!', 'Atualizado com sucesso!', 'success');
            return redirect()->route('admin.user.index');
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function destroy(int $id): RedirectResponse
    {
        try {
            $user = $this->user->findOrFail($id);

            $user->delete();

            Alert('Sucesso!', 'Excluído com sucesso!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }
}
