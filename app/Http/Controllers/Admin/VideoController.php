<?php

namespace App\Http\Controllers\Admin;

use App\Enums\TypeVideoEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoRequest;
use App\Models\CategoryVideo;
use App\Models\Video;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    private Video $video;
    private CategoryVideo $categoryVideo;

    public function __construct(Video $video, CategoryVideo $categoryVideo)
    {
        $this->video = $video;
        $this->categoryVideo = $categoryVideo;
    }

    public function index(): View
    {
        return view('admin.video.index', [
            'videos' => $this->video->with('category')->orderBy('created_at', 'DESC')->get()
        ]);
    }

    public function create(): View
    {
        return view('admin.video.form', [
            'categories' => $this->categoryVideo->select('id', 'name')->orderBy('name', 'DESC')->get(),
            'types' => TypeVideoEnum::ARRAY_TYPE_VIDEOS
        ]);
    }

    public function store(VideoRequest $request): RedirectResponse
    {
        $data = $request->all();

        /** @var UploadedFile|null $videoUpload */
        $videoUpload = $data['video_upload'] ?? null;

        /** @var UploadedFile|null $image */
        $image = $data['image'] ?? null;

        try {
            if ($videoUpload !== null) {
                if (!$videoSaved = $videoUpload->store('videos')) {
                    Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');

                    return redirect()
                        ->back()
                        ->withInput();
                }

                $data['video'] = $videoSaved;
            }

            if ($image !== null) {
                if (!$imageSaved = $image->store('images')) {
                    Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');

                    return redirect()
                        ->back()
                        ->withInput();
                }

                $data['image'] = $imageSaved;
            }

            $this->video->create($data);

            Alert('Sucesso!', 'Adicionado com sucesso!', 'success');
            return redirect()->route('admin.video.index');
        } catch (\Throwable $th) {

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function edit(Video $video): View
    {
        return view('admin.video.form', [
            'video' => $video,
            'categories' => $this->categoryVideo->select('id', 'name')->orderBy('name', 'DESC')->get(),
            'types' => TypeVideoEnum::ARRAY_TYPE_VIDEOS
        ]);
    }

    public function update(VideoRequest $request, Video $video): RedirectResponse
    {
        $data = $request->all();

        /** @var UploadedFile|null $videoUpload */
        $videoUpload = $data['video_upload'] ?? null;

        /** @var UploadedFile|null $image */
        $image = $data['image'] ?? null;
        try {
            if ($videoUpload !== null) {
                if (!$videoSaved = $videoUpload->store('videos')) {
                    Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');

                    return redirect()
                        ->back()
                        ->withInput();
                }

                $data['video'] = $videoSaved;
            }

            if ($image !== null) {
                if (!$imageSaved = $image->store('images')) {
                    Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');

                    return redirect()
                        ->back()
                        ->withInput();
                }

                $data['image'] = $imageSaved;
            }
            $video->update($data);

            Alert('Sucesso!', 'Atualizado com sucesso!', 'success');
            return redirect()->route('admin.video.index');
        } catch (\Throwable $th) {

            if (env('app_debug')) {
                dd($th->getMessage());
            }

            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function destroy(Video $video): RedirectResponse
    {
        try {
            if ($video->is_upload) {
                Storage::delete($video->video);
            }

            if ($video->image) {
                Storage::delete($video->image);
            }

            $video->delete();

            Alert('Sucesso!', 'Excluído com sucesso!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }
}
