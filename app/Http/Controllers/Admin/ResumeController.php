<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Resume;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ResumeController extends Controller
{
    private Resume $resume;

    public function __construct(Resume $resume)
    {
        $this->resume = $resume;
    }

    public function index(): View
    {
        return view('admin.resume.index', [
            'resumes' => $this->resume
                ->with('department')
                ->orderBy('id', 'DESC')
                ->get()
        ]);
    }

    public function downloadResume(Resume $resume): StreamedResponse
    {
        return Storage::download($resume->resume);
    }


}
