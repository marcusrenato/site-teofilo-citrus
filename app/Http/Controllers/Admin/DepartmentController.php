<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DepartmentController extends Controller
{
    private Department $department;

    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    public function index(): View
    {
        return view('admin.department.index', [
            'departments' => $this->department
                ->with('resumes')
                ->orderBy('id', 'DESC')
                ->get()
        ]);
    }

    public function create(): View
    {
        return view('admin.department.form');
    }

    public function store(DepartmentRequest $request): RedirectResponse
    {
        $data = $request->all();

        try {
            $this->department->create($data);

            Alert('Sucesso!', 'Adicionado com sucesso!', 'success');
            return redirect()->route('admin.department.index');
        } catch (\Throwable $th) {

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function edit(Department $department): View
    {
        return view('admin.department.form', [
            'department' => $department
        ]);
    }

    public function update(DepartmentRequest $request, Department $department): RedirectResponse
    {
        $data = $request->all();

        try {
            $department->update($data);

            Alert('Sucesso!', 'Atualizado com sucesso!', 'success');
            return redirect()->route('admin.department.index');
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }

    public function destroy(Department $department): RedirectResponse
    {
        try {
            $department->delete();

            Alert('Sucesso!', 'Excluído com sucesso!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert('Erro!', 'Ocorreu um erro. Tente novamente.', 'error');
            return redirect()
                ->back()
                ->withErro('Ocorreu um erro!')
                ->withInput();
        }
    }
}
