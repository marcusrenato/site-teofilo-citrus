<?php

namespace App\Http\Controllers\Site;

use App\Enums\RouteNameEnum;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;

class PartnershipController extends Controller
{
    public function __invoke(): View
    {
        return view(RouteNameEnum::ROUTE_NAME_PARTNERSHIP, [
            'pageName' => 'Motivos'
        ]);
    }
}
