<?php

namespace App\Http\Controllers\Site;

use App\Enums\TypeVideoEnum;
use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    private Video $video;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function __invoke(): View
    {
//        dd(
//            $this->video
//                ->with('category')
//                ->where('type_video', TypeVideoEnum::VIDEO_YOUTUBE)
//                ->orderBy('created_at')
//                ->first()->video_id
//        );
        return view('site.videos', [
            'pageName' => 'Vídeos',
            'videos' => $this->video
                ->with('category')
                ->orderBy('created_at', 'DESC')
                ->get()
        ]);
    }
}
