<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __invoke(): View
    {
        return view('site.company', [
            'pageName' => 'Empresa'
        ]);
    }
}
