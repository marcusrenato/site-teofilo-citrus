<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\ResumeRequest;
use App\Models\Department;
use App\Models\Resume;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use RealRashid\SweetAlert\Facades\Alert;

class ResumeController extends Controller
{
    private Resume $resume;
    private Department $department;

    public function __construct(Resume $resume, Department $department)
    {
        $this->resume = $resume;
        $this->department = $department;
    }

    public function index(): View
    {
        return view('site.carreiras', [
            'departments' => $this->department->select('id', 'name')->orderBy('name')->get(),
            'pageName' => 'Trabalhe Conosco'
        ]);
    }

    public function store(ResumeRequest $request): RedirectResponse
    {
        $dataResume = $request->all();
        $department = $this->department->where('id', $dataResume['department_id'])->first();

        /** @var UploadedFile $fileResume */
        $fileResume = $dataResume['resume'];

        if ($fileResume->isValid()) {
            $nameFile = str_replace(' ', '-', strtolower($dataResume['name'])) . date('d-m-Y-H:i');
            $nameFile .= '.' . $fileResume->extension();

            if (!$saved = $fileResume->storeAs('resumes', $nameFile)) {
                Alert('Ops. Algo deu errado', 'Não conseguimos salvar seu currículo, tente novamente agora ou mais tarde');
                return redirect()
                    ->back();
            }
            $dataResume['resume'] = $saved;
        }

        $this->resume->create($dataResume);

        Alert('Oba, deu tudo certo', 'Ficamos felizes com seu interesse, nossa equipe erá vê suas informações com muito cuidade.');

        $headers = "From: TS Citros <teofilo@tscitros.com.br>\n";
        $headers .= "Reply-To: TS Citros <no-reply@tscitros.com.br>\n";
        $headers .= "Return-Path: TS Citros <teofilo@tscitros.com.br>\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";

        mail(
            $department->email,
            'Recebemos um novo curriculo',
            'Olá, você recebeu um novo currículo. Acesse: ' . route('admin.resume.index') . ' para mais detalhes.',
            $headers
        );

        return redirect()
            ->back();
    }
}
