```sh
cp .env.example .env
docker-compose up -d
docker-compose run --rm app composer install
docker-compose run --rm app php artisan key:generate
docker-compose run --rm app php artisan migrate
```
