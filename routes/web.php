<?php

use App\Http\Controllers\Admin\ACL\RoleController;
use App\Http\Controllers\Admin\CategoryVideoController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ResumeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\VideoController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Site\CompanyController;
use App\Http\Controllers\Site\PartnershipController;
use App\Http\Controllers\Site\ResumeController as SiteResumeController;
use App\Http\Controllers\Site\VideoController as SiteVideoController;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Site
Route::name('site.')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('index');

//    Route::get('/carreiras', function() {
//        return view('site.carreiras');
//    });
//
    Route::get('/videos', SiteVideoController::class)->name('video');
    Route::get('empresa', CompanyController::class)->name('company');
    Route::get('motivos', PartnershipController::class)->name('partnership');

    Route::get(
        'produtos/mudas',
        fn (): View => view('site.plant_seedlings', ['pageName' => 'Mudas'])
    )->name('seedlings');

    Route::get(
        'produtos/porta-enxertos',
        fn (): View => view('site.grafts', ['pageName' => 'Porta-enxertos'])
    )->name('grafts');

    Route::get(
        'produtos/borbulhas',
        fn (): View => view('site.bubbles', ['pageName' => 'Borbulhas'])
    )->name('bubbles');

    Route::get(
        'contato',
        fn (): View => view('site.contact', ['pageName' => 'Contato'])
    )->name('contact');

    Route::get('carreiras', [SiteResumeController::class, 'index'])->name('career');
    Route::post('carreiras', [SiteResumeController::class, 'store'])->name('career-send');
});


Route::prefix('admin')->name('admin.')->group(function () {
    Route::group(
        ['middleware' => ['auth']],
        function () {
            // Dashboard
            Route::get('/', DashboardController::class)->name('dashboard.index');

            // Perfil
            Route::prefix('perfil')->name('profile.')->group(function () {
                Route::get('/', [ProfileController::class, 'index'])->name('index');
                Route::put('update', [ProfileController::class, 'update'])->name('update');
            });

            // Usuários
            Route::prefix('usuarios')->name('user.')->group(function () {
                Route::get('/', [UserController::class, 'index'])
                    ->name('index')
                    ->can('visualizar usuários');

                Route::group(['middleware' => ['can:cadastrar usuário']], function () {
                    Route::get('/cadastrar', [UserController::class, 'create'])->name('create');
                    Route::post('/cadastrar', [UserController::class, 'store'])->name('store');
                });

                Route::group(['middleware' => ['can:editar usuário']], function () {
                    Route::get('editar/{id}', [UserController::class, 'edit'])->name('edit');
                    Route::put('editar/{id}', [UserController::class, 'update'])->name('update');
                });

                Route::delete('excluir/{id}', [UserController::class, 'destroy'])
                    ->name('destroy')
                    ->can('excluir usuário');
            });

            // Cargos
            Route::prefix('cargos')->name('role.')->group(function () {

                Route::get('/', [RoleController::class, 'index'])
                    ->name('index')
                    ->can('visualizar cargos');

                Route::group(['middleware' => ['can:cadastrar cargo']], function () {
                    Route::get('/cadastrar', [RoleController::class, 'create'])->name('create');
                    Route::post('/cadastrar', [RoleController::class, 'store'])->name('store');
                });

                Route::group(['middleware' => ['can:editar cargo']], function () {
                    Route::get('editar/{id}', [RoleController::class, 'edit'])->name('edit');
                    Route::put('editar/{id}', [RoleController::class, 'update'])->name('update');
                });

                Route::delete('excluir/{id}', [RoleController::class, 'destroy'])
                    ->name('destroy')
                    ->can('excluir cargo');

            });

            // Departamentos
            Route::prefix('departamentos')->name('department.')->group(function () {
                Route::get('/', [DepartmentController::class, 'index'])
                    ->name('index')
                    ->can('visualizar departamentos');

                Route::group(['middleware' => ['can:cadastrar departamento']], function () {
                    Route::get('/cadastrar', [DepartmentController::class, 'create'])->name('create');
                    Route::post('/cadastrar', [DepartmentController::class, 'store'])->name('store');
                });

                Route::group(['middleware' => ['can:editar departamento']], function () {
                    Route::get('editar/{department}', [DepartmentController::class, 'edit'])->name('edit');
                    Route::put('editar/{department}', [DepartmentController::class, 'update'])->name('update');
                });

                Route::delete('excluir/{department}', [DepartmentController::class, 'destroy'])
                    ->name('destroy')
                    ->can('excluir departamento');
            });

            // Currículos
            Route::prefix('curriculos')->name('resume.')->group(function () {
                Route::get('/', [ResumeController::class, 'index'])
                    ->name('index')
                    ->can('visualizar curriculos');

                Route::get('/download/{resume}', [ResumeController::class, 'downloadResume'])
                    ->name('download')
                    ->can('baixar curriculo');

                Route::delete('excluir/{curriculo}', [DepartmentController::class, 'destroy'])
                    ->name('destroy')
                    ->can('excluir curriculo');
            });

            // Categorias de vídeos
            Route::prefix('categorias-videos')->name('category_video.')->group(function () {
                Route::get('/', [CategoryVideoController::class, 'index'])
                    ->name('index')
                    ->can('visualizar categorias de video');

                Route::group(['middleware' => ['can:cadastrar categoria de video']], function () {
                    Route::get('/cadastrar', [CategoryVideoController::class, 'create'])->name('create');
                    Route::post('/cadastrar', [CategoryVideoController::class, 'store'])->name('store');
                });

                Route::group(['middleware' => ['can:editar categoria de video']], function () {
                    Route::get('editar/{categoryVideo}', [CategoryVideoController::class, 'edit'])->name('edit');
                    Route::put('editar/{categoryVideo}', [CategoryVideoController::class, 'update'])->name('update');
                });

                Route::delete('excluir/{categoryVideo}', [CategoryVideoController::class, 'destroy'])
                    ->name('destroy')
                    ->can('excluir categoria de video');
            });

            // Vídeos
            Route::prefix('videos')->name('video.')->group(function () {
                Route::get('/', [VideoController::class, 'index'])
                    ->name('index')
                    ->can('visualizar videos');

                Route::group(['middleware' => ['can:cadastrar video']], function () {
                    Route::get('/cadastrar', [VideoController::class, 'create'])->name('create');
                    Route::post('/cadastrar', [VideoController::class, 'store'])->name('store');
                });

                Route::group(['middleware' => ['can:editar video']], function () {
                    Route::get('editar/{video}', [VideoController::class, 'edit'])->name('edit');
                    Route::put('editar/{video}', [VideoController::class, 'update'])->name('update');
                });

                Route::delete('excluir/{video}', [VideoController::class, 'destroy'])
                    ->name('destroy')
                    ->can('excluir video');
            });

        }
    );
});


Auth::routes();
