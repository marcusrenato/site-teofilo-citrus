<?php

namespace Database\Seeders;

use App\Models\CategoryVideo;
use Illuminate\Database\Seeder;

class CategoryVideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            [
                'name' => 'Entrevista',
                'color' => '#f7941e'
            ],
            [
                'name' => 'Dicas',
                'color' => '#869c21'
            ],
            [
                'name' => 'Institucional',
                'color' => '#ed1c24'
            ],
        ];

        foreach ($categories as $category) {
            CategoryVideo::create($category);
        }
    }
}
