<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $departments = [
            [
                'name' => 'Gerência',
                'email' => 'gerencia@teofilocitrus.com.br'
            ],
            [
                'name' => 'Financeiro',
                'email' => 'financeiro@teofilocitrus.com.br'
            ],
            [
                'name' => 'Recursos Humanos',
                'email' => 'rh@teofilocitrus.com.br'
            ],
            [
                'name' => 'Compras',
                'email' => 'compras@teofilocitrus.com.br'
            ],
            [
                'name' => 'Vendas',
                'email' => 'vendas@teofilocitrus.com.br'
            ],
            [
                'name' => 'Análise',
                'email' => 'analise@teofilocitrus.com.br'
            ],
        ];

        foreach ($departments as $department) {
            Department::create($department);
        }
    }
}
