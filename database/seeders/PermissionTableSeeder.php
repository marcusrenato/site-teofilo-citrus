<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // add in role permissions
        $super = Role::updateOrCreate(['name' => 'Super Administrador']);
        $admin = Role::updateOrCreate(['name' => 'Administrador']);

        $rules = [
            // Dashboard
            [
                'name' => 'visualizar dashboard'
            ],
            // Usuários
            [
                'name' => 'visualizar usuários'
            ],
            [
                'name' => 'cadastrar usuário'
            ],
            [
                'name' => 'editar usuário'
            ],
            [
                'name' => 'excluir usuário'
            ],
            // Cargos
            [
                'name' => 'visualizar cargos'
            ],
            [
                'name' => 'cadastrar cargo'
            ],
            [
                'name' => 'editar cargo'
            ],
            [
                'name' => 'excluir cargo'
            ],
            // Departamentos
            [
                'name' => 'visualizar departamentos'
            ],
            [
                'name' => 'cadastrar departamento'
            ],
            [
                'name' => 'editar departamento'
            ],
            [
                'name' => 'excluir departamento'
            ],
            // Currículos
            [
                'name' => 'visualizar curriculos'
            ],
            [
                'name' => 'baixar curriculo'
            ],
            [
                'name' => 'excluir curriculo'
            ],
            // Categorias de vídeos
            [
                'name' => 'visualizar categorias de video'
            ],
            [
                'name' => 'cadastrar categoria de video'
            ],
            [
                'name' => 'editar categoria de video'
            ],
            [
                'name' => 'excluir categoria de video'
            ],
            // Vídeos
            [
                'name' => 'visualizar videos'
            ],
            [
                'name' => 'cadastrar video'
            ],
            [
                'name' => 'editar video'
            ],
            [
                'name' => 'excluir video'
            ],
        ];

        foreach ($rules as $rule) {
            $permission = Permission::updateOrCreate([
                'name' => $rule['name']
            ]);

            $this->command->info('✓ ' . ucfirst($permission->name) . ' adicionada com sucesso');

            $super->givePermissionTo($permission);
            $admin->givePermissionTo($permission);
        }


         $user = User::updateOrcreate([
             'name' => 'Super Admin',
             'email' => 'super@adm.com'
         ], [
             'password' => bcrypt('abc#123')
         ]);

         $user->assignRole($super);

        $user = User::updateOrcreate([
             'name' => 'Administrador',
             'email' => 'adm@adm.com'
         ], [
             'password' => bcrypt('abc#123')
         ]);

         $user->assignRole($admin);
    }
}
