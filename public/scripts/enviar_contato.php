﻿<?php header("Content-Type: text/html; charset=utf-8",true) ?>
<?php

$redirecionar = "http://www.teofiloescholl.com.br/obrigado.html";

function enviar_email($para, $assunto, $msg) {
	$headers = "From: TS Citros <teofilo@tscitros.com.br>\n";
	$headers .= "Reply-To: TS Citros <teofilo@tscitros.com.br>\n";
	$headers .= "Return-Path: TS Citros <teofilo@tscitros.com.br>\n";
	$headers .= "Content-type: text/html; charset=utf-8\n";
	mail($para, $assunto, $msg, $headers);
}

///////////////////////DECLARAÇÃO DE VARIÁVEIS///////////////////////////////////////////////////////////////////////

$datahora = date("d/m/y - H:i:s");

$nome = urldecode($_POST["nome"]);
$email = urldecode($_POST["email"]);
$assunto = urldecode($_POST["motivo"]);
$ddd = urldecode($_POST["ddd"]);
$telefone = urldecode($_POST["telefone"]);
$mensagem = urldecode($_POST["mensagem"]);


///////////////////////ENVIO DOS DADOS POR E-MAIL///////////////////////////////////////////////////////////////////////

$destino = "compras@tscitros.com.br";
$assunto = "Mensagem enviada pelo site da TS Citros";


$msg = "<strong>DATA: $datahora</strong><br><br>
NOME: $nome<br>
E-MAIL: $email<br>
TELEFONE: $telefone<br>
MENSAGEM: $mensagem<br>";

enviar_email($destino, $assunto, $msg);

?>
<script>
alert("Seu formulario foi enviado com sucesso! Em breve nosso departamento de atendimento, entrara em contato com voce.");
window.location = 'http://www.tscitros.com.br';
</script>
