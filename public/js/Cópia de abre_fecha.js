var currentItem = null;
var undoTimerHodler = false;
$(document).ready(
	function()
	{
		$('#menu div')
			.bind('mouseover', doItem);
		$('#menu')
			.bind('mouseout', undoItem);
	}
);
function doItem()
{
	if(undoTimerHodler)
		window.clearTimeout(undoTimerHodler);
	if(currentItem) {
		$(currentItem).animate({width: 60}, 200);
	} else {
		$('#menu div').not(this).animate({width: 60}, 200);
	}
	currentItem = this;
	$(currentItem).animate({width: 265}, 200);
}
function undoItem()
{
	undoTimerHodler = window.setTimeout(undoTimer, 100);
}
function undoTimer()
{
	$('#menu div').animate({width: 60}, 200);
	currentItem = null;
}